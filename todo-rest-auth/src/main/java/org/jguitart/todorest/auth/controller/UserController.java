package org.jguitart.todorest.auth.controller;

import org.jguitart.todorest.auth.model.TodoAuthException;
import org.jguitart.todorest.auth.service.UserService;
import org.jguitart.todorest.model.auth.User;
import org.jguitart.todorest.model.auth.UserImpl;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService service;


    private UserService getService() {
        return this.service;
    }


    @RequestMapping(path ="", method= RequestMethod.POST)
    public User create(@RequestBody UserImpl todoTask) throws TodoAuthException {
        return this.getService().save(todoTask);
    }

    @RequestMapping(path ="", method= RequestMethod.PUT)
    public User update(@RequestBody UserImpl todoTask) throws TodoAuthException {
        if(todoTask.getId() == null) {
            throw new TodoAuthException("error.id.mandatory");
        }
        return this.getService().save(todoTask);
    }

    @RequestMapping(path ="/{id}", method= RequestMethod.DELETE)
    public Map<String, Object> delete(@PathVariable String id) {
        getService().delete(id);
        return new HashMap<>();
    }

    @RequestMapping(path ="/{id}", method= RequestMethod.GET)
    public User get(@PathVariable String id) throws TodoAuthException {
        return getService().get(id);
    }

    @RequestMapping(path ="/search", method= RequestMethod.POST)
    public SearchPage<User> search(@RequestBody SearchParams params) throws TodoAuthException {
        return getService().search(params);
    }

}
