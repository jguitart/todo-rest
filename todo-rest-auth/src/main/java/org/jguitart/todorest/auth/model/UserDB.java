package org.jguitart.todorest.auth.model;

import org.jguitart.todorest.model.auth.User;
import org.jguitart.todorest.model.auth.UserImpl;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@CompoundIndex(name = "username_idx", def = "{'username': 1}", unique =  true)
public class UserDB extends UserImpl {

    public void apply(User user) {

        this.setUsername(user.getUsername());
        this.setPassword(user.getPassword());

    }
}
