package org.jguitart.todorest.auth.repository;

import org.jguitart.todorest.auth.model.UserDB;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<UserDB, String> {

}
