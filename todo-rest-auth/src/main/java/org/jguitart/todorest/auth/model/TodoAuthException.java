package org.jguitart.todorest.auth.model;

public class TodoAuthException extends Exception{

    public TodoAuthException(String s) {
        super(s);
    }
}
