package org.jguitart.todorest.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TodoRestAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoRestAuthApplication.class, args);
    }
}
