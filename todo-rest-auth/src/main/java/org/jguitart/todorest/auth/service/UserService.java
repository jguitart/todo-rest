package org.jguitart.todorest.auth.service;

import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoWriteException;
import org.jguitart.todorest.auth.model.TodoAuthException;
import org.jguitart.todorest.auth.model.UserDB;
import org.jguitart.todorest.auth.repository.UserRepository;
import org.jguitart.todorest.model.auth.User;
import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public UserRepository getRepository() {
        return repository;
    }

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public User save(User user) throws TodoAuthException {
        UserDB instance = null;

        if(user.getUsername()==null || "".equals(user.getUsername())) {
            throw new TodoAuthException("error.username.mandatory");
        }

        if(user.getPassword()==null || "".equals(user.getPassword())) {
            throw new TodoAuthException("error.password.mandatory");
        }

        if(user.getId()!=null) {
            Optional<UserDB> opt = getRepository().findById(user.getId());
            if(!opt.isPresent()) {
                throw new TodoAuthException("error.user.not_found");
            }
            instance = opt.get();
        } else {
            instance = new UserDB();
            instance.setCreatedDate(new Date());
        }

        instance.apply(user);
        instance.setLastUpdate(new Date());
        UserDB result = null;
        try {
            result = this.repository.save(instance);
        } catch (Exception e) {
            if(e.getMessage().contains("E11000")) {
                throw new TodoAuthException("error.user.already_exists");
            } else {
                throw new TodoAuthException(e.getMessage());
            }
        }

        return result;
    }

    public User get(String id) throws TodoAuthException {
        UserDB result = null;
        Optional<UserDB> opt = this.getRepository().findById(id);
        if(opt.isPresent()) {
            result = opt.get();
        } else {
            throw new TodoAuthException("error.user.not_found");
        }
        return result;
    }

    public void delete(String id) {
        getRepository().deleteById(id);
    }

    public SearchPage<User> search(SearchParams params) throws TodoAuthException {

        Query query = new Query();
        if(params.getTerm()!=null) {
            query.addCriteria(Criteria.where("username").regex(params.getTerm(), "i"));
        }

        if(params.getFilters()!=null && !params.getFilters().isEmpty()) {
            for(SearchFilter filter: params.getFilters()) {
                query.addCriteria(this.getFilterCriteria(filter));
            }
        }

        long count = getMongoTemplate().count(query, UserDB.class);
        query.with(getPageable(params));
        List<UserDB> itemsDB = getMongoTemplate().find(query, UserDB.class);
        List<User> items = itemsDB.stream().map(itemDB -> (User) itemDB).collect(Collectors.toList());
        SearchPage<User> searchPage = new SearchPage<>();
        searchPage.setCount(count);
        searchPage.setItems(items);
        return searchPage;

    }


    private Pageable getPageable(SearchParams params) {
        if(params.getSortColumn() == null || "".equals(params.getSortColumn())) {
            params.setSortColumn("createdDate");
            params.setSortAsc(false);
        }

        if(params.getSize() == 0) {
            params.setSize(20);
        }

        Sort.Direction direction = params.isSortAsc() ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = new Sort(direction, params.getSortColumn());
        return PageRequest.of(params.getPage(), params.getSize(), sort);
    }

    private Criteria getFilterCriteria(SearchFilter filter) throws TodoAuthException {
        Criteria criteria = null;
        switch (filter.getName()) {
            case "withUsername":
                criteria = this.getWithUsernameCriteria(filter);
                break;
            default:
                throw new TodoAuthException("error.search.unknown_filter");
        }
        return criteria;
    }

    private Criteria getWithUsernameCriteria(SearchFilter filter) {
        String value = (String) filter.getValue();
        return Criteria.where("username").is(value);
    }


}
