package org.jguitart.todorest.auth;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationBaseTest {

    private static final MongodStarter starter = MongodStarter.getDefaultInstance();

    private final int MONGO_TEST_PORT = 27909;
    private final String MONGO_TEST_HOST = "localhost";

    private MongodExecutable _mongodExe;
    private MongodProcess _mongod;

    @LocalServerPort
    protected int port;

    @Before
    public void before() throws IOException {
        //Instance embedded mongo
        _mongodExe = starter.prepare(new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(MONGO_TEST_HOST, MONGO_TEST_PORT, Network.localhostIsIPv6()))
                .build());

        _mongod = _mongodExe.start();
    }

    @After
    public void after() {
        _mongod.stop();
        _mongodExe.stop();
    }

    @Test
    public void testMongo() {
        Assert.assertNotNull(this._mongod);
    }

}
