package org.jguitart.todorest.auth.controller;

import org.jguitart.todorest.auth.IntegrationBaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;


public class EchoControllerTest extends IntegrationBaseTest {


    @Test
    public void testEcho() {
        String text = String.format("Hi_%d", System.currentTimeMillis());
        String url = String.format("/api/echo/%s", text);
        given().port(port).when().get(url).then().body("message", is(text));
    }

}
