package org.jguitart.todorest.auth.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.jguitart.todorest.auth.IntegrationBaseTest;
import org.jguitart.todorest.auth.model.UserDB;
import org.jguitart.todorest.auth.repository.UserRepository;
import org.jguitart.todorest.model.auth.UserImpl;
import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchParams;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class UserControllerTest extends IntegrationBaseTest {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    UserRepository repository;

    @Test
    public void create() throws JsonProcessingException {
        String url = "/api/user";
        UserImpl user = new UserImpl();
        user.setUsername("user_test_to_delete");
        user.setPassword("password");
        user.setCreatedDate(new Date());
        user.setLastUpdate(new Date());
        String payload = mapper.writeValueAsString(user);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("username", equalTo(user.getUsername()))
                .body("password", equalTo(user.getPassword()))
                .body("createdDate", not(user.getCreatedDate().getTime()))
                .body("lastUpdate", not(user.getLastUpdate().getTime()))
                .extract().path("id");

        String getUrl = String.format("/api/user/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("username", equalTo(user.getUsername()))
                .body("password", equalTo(user.getPassword()))
                .body("createdDate", not(user.getCreatedDate().getTime()))
                .body("lastUpdate", not(user.getLastUpdate().getTime()));

    }

    @Test
    public void createErrors() throws JsonProcessingException {
        String url = "/api/user";
        UserImpl user = new UserImpl();
        String payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.username.mandatory"));

        user.setUsername("");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.username.mandatory"));

        user.setUsername("username");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.password.mandatory"));

        user.setPassword("");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.password.mandatory"));

        user.setPassword("password");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200);


        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.user.already_exists"));


    }

    @Test
    public void getErrors() {
        String getUrl = String.format("/api/user/%s", String.valueOf(System.currentTimeMillis()));

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.user.not_found"));
    }

    @Test
    public void update() throws JsonProcessingException {
        //Create task
        String url = "/api/user";
        UserImpl original = new UserImpl();
        original.setCreatedDate(new Date());
        original.setLastUpdate(new Date());
        original.setUsername("user_test_todelete");
        original.setPassword("password");
        String payload = mapper.writeValueAsString(original);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("username", equalTo(original.getUsername()))
                .body("password", equalTo(original.getPassword()))
                .body("createdDate", not(original.getCreatedDate().getTime()))
                .body("lastUpdate", not(original.getLastUpdate().getTime()))
                .extract().path("id");

        UserImpl expected = new UserImpl();
        expected.setId(id);
        expected.setUsername("user_test_todelete_updated");
        expected.setPassword("password_updated");
        payload = mapper.writeValueAsString(expected);

        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("username", equalTo(expected.getUsername()))
                .body("password", equalTo(expected.getPassword()));

        String getUrl = String.format("/api/user/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("username", equalTo(expected.getUsername()))
                .body("password", equalTo(expected.getPassword()));

    }

    @Test
    public void updateErrors() throws JsonProcessingException {

        String url = "/api/user";

        UserImpl user = new UserImpl();
        String payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.id.mandatory"));

        user.setId("mockedId");
        payload = mapper.writeValueAsString(user);


        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.username.mandatory"));

        user.setUsername("");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.username.mandatory"));

        user.setUsername("username");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.password.mandatory"));

        user.setPassword("");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.password.mandatory"));

        user.setPassword("password");
        payload = mapper.writeValueAsString(user);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.user.not_found"));

    }

    @Test
    public void searchTerm() throws JsonProcessingException {

        String url = "/api/user/search";

        for(int i = 0; i<20; i++) {

            UserDB user = new UserDB();
            user.setCreatedDate(new Date());
            user.setLastUpdate(new Date());
            user.setUsername(String.format("user_search_%d", i));
            user.setPassword("password");
            this.repository.save(user);
        }

        // Search by term
        SearchParams searchByTerm = new SearchParams();
        searchByTerm.setTerm("user_search_1");
        String payload = mapper.writeValueAsString(searchByTerm);
        List<Map<String, Object>> items = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("count", equalTo(11))
                .extract().path("items");

        for(Map item: items) {
            String username = (String) item.get("username");
            Assertions.assertThat(username).contains("user_search_1");
        }

        //Search withUsername
        SearchParams searchWithusername = new SearchParams();
        SearchFilter withUsernameFilter = new SearchFilter();
        withUsernameFilter.setName("withUsername");
        withUsernameFilter.setValue("user_search_0");
        List<SearchFilter> filters = new ArrayList<>();
        filters.add(withUsernameFilter);
        searchWithusername.setFilters(filters);
        payload = mapper.writeValueAsString(searchWithusername);
        items = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("count", equalTo(1))
                .extract().path("items");

        for(Map item: items) {
            String username = (String) item.get("username");
            Assertions.assertThat(username).isEqualTo("user_search_0");
        }

    }

    @Test
    public void delete() throws JsonProcessingException {
        String url = "/api/user";
        UserImpl user = new UserImpl();
        user.setUsername("user_delete_test");
        user.setPassword("password");
        user.setCreatedDate(new Date());
        user.setLastUpdate(new Date());
        String payload = mapper.writeValueAsString(user);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("username", equalTo(user.getUsername()))
                .body("password", equalTo(user.getPassword()))
                .body("createdDate", not(user.getCreatedDate().getTime()))
                .body("lastUpdate", not(user.getLastUpdate().getTime()))
                .extract().path("id");

        String getUrl = String.format("/api/user/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("username", equalTo(user.getUsername()))
                .body("password", equalTo(user.getPassword()))
                .body("createdDate", not(user.getCreatedDate().getTime()))
                .body("lastUpdate", not(user.getLastUpdate().getTime()));

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .delete(getUrl)
                .then()
                .statusCode(200);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.user.not_found"));
    }

}
