package org.jguitart.todorest.tasks.service;

import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.tasks.model.TodoTaskDB;
import org.jguitart.todorest.tasks.model.TodoTaskException;
import org.jguitart.todorest.tasks.repository.TaskRepository;
import org.jguitart.todorest.model.task.TodoTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskService {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private TaskRepository getRepository() {
        return this.repository;
    }

    private MongoTemplate getMongoTemplate() {
        return this.mongoTemplate;
    }

    public TodoTask save(TodoTask task) throws TodoTaskException {

        if(task.getUserId() == null || "".equals(task.getUserId())) {
            throw new TodoTaskException("error.userId.mandatory");
        }

        if(task.getText() == null || "".equals(task.getText())) {
            throw new TodoTaskException("error.text.mandatory");
        }

        TodoTaskDB instance = null;
        if(task.getId()!=null) {
            Optional<TodoTaskDB> opt = this.getRepository().findById(task.getId());
            if(opt.isPresent()) {
                instance = opt.get();
            } else {
                throw new TodoTaskException("error.task.not_found");
            }
        } else {
            //Create
            instance = new TodoTaskDB();
            instance.setCreatedDate(new Date());
        }

        instance.apply(task);
        instance.setLastUpdate(new Date());
        TodoTaskDB result = this.getRepository().save(instance);
        return result;
    }

    public TodoTask get(String id) throws TodoTaskException {
        TodoTask result = null;
        Optional<TodoTaskDB> opt = this.getRepository().findById(id);
        if(opt.isPresent()) {
            result = opt.get();
        } else {
            throw new TodoTaskException("error.task.not_found");
        }
        return result;
    }


    public void delete(String id) {
        getRepository().deleteById(id);
    }

    public SearchPage<TodoTask> search(SearchParams params) throws TodoTaskException {
        Query query = new Query();

        if(params.getTerm()!=null && !"".equals(params.getTerm())) {
            query.addCriteria(Criteria.where("text").regex(params.getTerm(), "i"));
        }

        if(params.getFilters()!=null && !params.getFilters().isEmpty()) {
            for(SearchFilter filter: params.getFilters()) {
                query.addCriteria(this.getFilterCriteria(filter));
            }
        }

        long count = getMongoTemplate().count(query, TodoTaskDB.class);
        query.with(getPageable(params));
        List<TodoTaskDB> itemsDB = getMongoTemplate().find(query, TodoTaskDB.class);
        List<TodoTask> items = itemsDB.stream().map(itemDB -> (TodoTask) itemDB).collect(Collectors.toList());
        SearchPage<TodoTask> searchPage = new SearchPage<>();
        searchPage.setCount(count);
        searchPage.setItems(items);
        return searchPage;
    }

    private Criteria getFilterCriteria(SearchFilter filter) throws TodoTaskException {
        Criteria criteria = null;
        switch (filter.getName()) {
            case "byUserId":
                criteria = this.getByUserIdCriteria(filter);
                break;
            case "byCompleted":
                criteria = this.getByCompletedCriteria(filter);
                break;
            default:
                throw new TodoTaskException("error.search.unknown_filter");
        }
        return criteria;
    }

    private Criteria getByUserIdCriteria(SearchFilter filter) {
        String userId = (String) filter.getValue();
        return Criteria.where("userId").is(userId);
    }

    private Criteria getByCompletedCriteria(SearchFilter filter) {
        boolean completed = (Boolean) filter.getValue();
        return Criteria.where("completed").is(completed);
    }

    private Pageable getPageable(SearchParams params) {
        if(params.getSortColumn() == null || "".equals(params.getSortColumn())) {
            params.setSortColumn("createdDate");
            params.setSortAsc(false);
        }

        if(params.getSize() == 0) {
            params.setSize(20);
        }

        Sort.Direction direction = params.isSortAsc() ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = new Sort(direction, params.getSortColumn());
        return PageRequest.of(params.getPage(), params.getSize(), sort);
    }

}
