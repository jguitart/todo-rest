package org.jguitart.todorest.tasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoRestTasksApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoRestTasksApplication.class, args);
    }
}
