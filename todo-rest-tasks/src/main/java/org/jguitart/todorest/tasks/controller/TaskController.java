package org.jguitart.todorest.tasks.controller;

import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.model.task.TodoTask;
import org.jguitart.todorest.model.task.TodoTaskImpl;
import org.jguitart.todorest.tasks.model.TodoTaskException;
import org.jguitart.todorest.tasks.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    private TaskService service;


    private TaskService getService() {
        return this.service;
    }


    @RequestMapping(path ="", method= RequestMethod.POST)
    public TodoTask create(@RequestBody TodoTaskImpl todoTask) throws TodoTaskException {
        return this.getService().save(todoTask);
    }

    @RequestMapping(path ="", method= RequestMethod.PUT)
    public TodoTask update(@RequestBody TodoTaskImpl todoTask) throws TodoTaskException {
        if(todoTask.getId() == null) {
            throw new TodoTaskException("error.id.mandatory");
        }
        return this.getService().save(todoTask);
    }

    @RequestMapping(path ="/{id}", method= RequestMethod.DELETE)
    public Map<String, Object> delete(@PathVariable String id) {
        getService().delete(id);
        return new HashMap<>();
    }

    @RequestMapping(path ="/{id}", method= RequestMethod.GET)
    public TodoTask get(@PathVariable String id) throws TodoTaskException {
        return getService().get(id);
    }

    @RequestMapping(path ="/search", method= RequestMethod.POST)
    public SearchPage<TodoTask> search(@RequestBody SearchParams params) throws TodoTaskException {
        return getService().search(params);
    }

}
