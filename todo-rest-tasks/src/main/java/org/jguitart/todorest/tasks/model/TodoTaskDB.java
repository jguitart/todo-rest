package org.jguitart.todorest.tasks.model;

import org.jguitart.todorest.model.task.TodoTask;
import org.jguitart.todorest.model.task.TodoTaskImpl;

public class TodoTaskDB extends TodoTaskImpl {

    public TodoTaskDB() {}

    public void apply(TodoTask todoTask) {
        this.setId(todoTask.getId());
        this.setCompleted(todoTask.isCompleted());
        this.setText(todoTask.getText());
        this.setUserId(todoTask.getUserId());

        //Whe do not apply createdDate and LastUpdate. These are for inner control.
    }

}
