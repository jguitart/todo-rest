package org.jguitart.todorest.tasks.controller;

import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;

@RestController
@RequestMapping("/api/echo")
public class EchoController {

    @RequestMapping(path = "/{message}", method = RequestMethod.GET)
    public LinkedHashMap<String, String> echo(@PathVariable String message) throws Exception {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        result.put("message", message);
        result.put("timestamp", String.valueOf(System.currentTimeMillis()));
        return result;
    }

}
