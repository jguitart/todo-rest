package org.jguitart.todorest.tasks.repository;

import org.jguitart.todorest.tasks.model.TodoTaskDB;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskRepository extends MongoRepository<TodoTaskDB, String> {
}
