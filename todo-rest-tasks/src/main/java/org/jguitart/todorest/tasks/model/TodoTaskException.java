package org.jguitart.todorest.tasks.model;

public class TodoTaskException extends Exception {

    public TodoTaskException(String message) {
        super(message);
    }
}
