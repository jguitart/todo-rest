package org.jguitart.todorest.tasks.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.model.task.TodoTaskImpl;
import org.jguitart.todorest.tasks.IntegrationBaseTest;
import org.jguitart.todorest.tasks.model.TodoTaskDB;
import org.jguitart.todorest.tasks.repository.TaskRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TaskControllerTest extends IntegrationBaseTest {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    TaskRepository repository;

    @Test
    public void create() throws JsonProcessingException {
        String url = "/api/task";
        TodoTaskImpl task = new TodoTaskImpl();
        task.setCompleted(true);
        task.setCreatedDate(new Date());
        task.setLastUpdate(new Date());
        task.setUserId("mockUserId");
        task.setText("Text");
        String payload = mapper.writeValueAsString(task);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
            .when()
                .post(url)
            .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("text", equalTo(task.getText()))
                .body("userId", equalTo(task.getUserId()))
                .body("completed", equalTo(task.isCompleted()))
                .body("createdDate", not(task.getCreatedDate().getTime()))
                .body("lastUpdate", not(task.getLastUpdate().getTime()))
                .extract().path("id");

        String getUrl = String.format("/api/task/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("text", equalTo(task.getText()))
                .body("userId", equalTo(task.getUserId()))
                .body("completed", equalTo(task.isCompleted()))
                .body("createdDate", not(task.getCreatedDate().getTime()))
                .body("lastUpdate", not(task.getLastUpdate().getTime()));

    }

    @Test
    public void createErrors() throws JsonProcessingException {
        String url = "/api/task";
        TodoTaskImpl task = new TodoTaskImpl();
        task.setCompleted(true);
        task.setCreatedDate(new Date());
        task.setLastUpdate(new Date());
        String payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.userId.mandatory"));



        task.setUserId("");


        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.userId.mandatory"));

        task.setUserId("mockedUserId");

        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.text.mandatory"));

        task.setText("");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.text.mandatory"));

        task.setText("text");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200);
    }

    @Test
    public void getErrors() {
        String getUrl = String.format("/api/task/%s", String.valueOf(System.currentTimeMillis()));

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.task.not_found"));
    }

    @Test
    public void update() throws JsonProcessingException {
        //Create task
        String url = "/api/task";
        TodoTaskImpl original = new TodoTaskImpl();
        original.setCompleted(true);
        original.setCreatedDate(new Date());
        original.setLastUpdate(new Date());
        original.setUserId("mockUserId");
        original.setText("Text");
        String payload = mapper.writeValueAsString(original);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("text", equalTo(original.getText()))
                .body("userId", equalTo(original.getUserId()))
                .body("completed", equalTo(original.isCompleted()))
                .body("createdDate", not(original.getCreatedDate().getTime()))
                .body("lastUpdate", not(original.getLastUpdate().getTime()))
                .extract().path("id");

        TodoTaskImpl expected = new TodoTaskImpl();
        expected.setId(id);
        expected.setCompleted(false);
        expected.setUserId("mockUserId_updated");
        expected.setText("Text_updated");
        payload = mapper.writeValueAsString(expected);

        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("text", equalTo(expected.getText()))
                .body("userId", equalTo(expected.getUserId()))
                .body("completed", equalTo(expected.isCompleted()));

        String getUrl = String.format("/api/task/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("text", equalTo(expected.getText()))
                .body("userId", equalTo(expected.getUserId()))
                .body("completed", equalTo(expected.isCompleted()));

    }

    @Test
    public void updateErrors() throws JsonProcessingException {

        String url = "/api/task";

        TodoTaskImpl task = new TodoTaskImpl();
        String payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.id.mandatory"));

        task.setId("mockedId");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.userId.mandatory"));


        task.setUserId("mockedUserId");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.text.mandatory"));

        task.setText("");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.text.mandatory"));

        task.setText("text");
        payload = mapper.writeValueAsString(task);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .put(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.task.not_found"));

    }

    @Test
    public void searchTerm() throws JsonProcessingException {

        String url = "/api/task/search";

        for(int i = 0; i<20; i++) {
            int userNum = i % 2;
            TodoTaskDB task = new TodoTaskDB();
            task.setCompleted(userNum == 0);
            task.setCreatedDate(new Date());
            task.setLastUpdate(new Date());
            task.setUserId(String.format("user_search_id_%d", userNum));
            task.setText(String.format("text_search_%d", i));
            this.repository.save(task);
        }

        // Search by term
        SearchParams searchByTerm = new SearchParams();
        searchByTerm.setTerm("text_search_1");
        String payload = mapper.writeValueAsString(searchByTerm);
        List<Map<String, Object>> items = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("count", equalTo(11))
                .extract().path("items");

        for(Map item: items) {
            String text = (String) item.get("text");
            Assertions.assertThat(text.contains("text_search_1")).isTrue();
        }

        //Search by User Id
        SearchParams searchByUserId = new SearchParams();
        SearchFilter filter = new SearchFilter();
        filter.setName("byUserId");
        filter.setValue("user_search_id_1");
        List<SearchFilter> filters = new ArrayList<>();
        filters.add(filter);
        searchByUserId.setFilters(filters);

        payload = mapper.writeValueAsString(searchByUserId);
        List<Map<String, Object>> itemsUserIdSearch = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("count", equalTo(10))
                .extract().path("items");

        for(Map item: itemsUserIdSearch) {
            String userId = (String) item.get("userId");
            Assertions.assertThat(userId).isEqualTo(filter.getValue());
        }

        //Search by completed
        SearchParams searchByCompleted = new SearchParams();
        filter = new SearchFilter();
        filter.setName("byCompleted");
        filter.setValue(true);
        filters = new ArrayList<>();
        filters.add(filter);
        searchByCompleted.setFilters(filters);
        payload = mapper.writeValueAsString(searchByCompleted);
        List<Map<String, Object>> itemsCompleted = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("count", equalTo(11))
                .extract().path("items");

        for(Map item: itemsCompleted) {
            boolean completed = (Boolean) item.get("completed");
            Assertions.assertThat(completed).isTrue();
        }

    }




    @Test
    public void searchErrors() throws JsonProcessingException {

        String url = "/api/task/search";


        SearchParams searchErrorFilter = new SearchParams();
        searchErrorFilter.setTerm("text_search_1");

        SearchFilter filter = new SearchFilter();
        filter.setName("no_filter");
        filter.setValue("doesn't matter");
        List<SearchFilter> filters = new ArrayList<>();
        filters.add(filter);
        searchErrorFilter.setFilters(filters);

        String payload = mapper.writeValueAsString(searchErrorFilter);
        given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.search.unknown_filter"));
    }

    @Test
    public void delete() throws JsonProcessingException {
        String url = "/api/task";
        TodoTaskImpl task = new TodoTaskImpl();
        task.setCompleted(true);
        task.setCreatedDate(new Date());
        task.setLastUpdate(new Date());
        task.setUserId("mockUserId");
        task.setText("Text");
        String payload = mapper.writeValueAsString(task);
        String id = given()
                .port(port)
                .contentType("application/json")
                .body(payload)
                .when()
                .post(url)
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("text", equalTo(task.getText()))
                .body("userId", equalTo(task.getUserId()))
                .body("completed", equalTo(task.isCompleted()))
                .body("createdDate", not(task.getCreatedDate().getTime()))
                .body("lastUpdate", not(task.getLastUpdate().getTime()))
                .extract().path("id");

        String getUrl = String.format("/api/task/%s", id);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(200)
                .body("id", equalTo(id))
                .body("text", equalTo(task.getText()))
                .body("userId", equalTo(task.getUserId()))
                .body("completed", equalTo(task.isCompleted()))
                .body("createdDate", not(task.getCreatedDate().getTime()))
                .body("lastUpdate", not(task.getLastUpdate().getTime()));


        given()
                .port(port)
                .contentType("application/json")
                .when()
                .delete(getUrl)
                .then()
                .statusCode(200);

        given()
                .port(port)
                .contentType("application/json")
                .when()
                .get(getUrl)
                .then()
                .statusCode(500)
                .body("message", equalTo("error.task.not_found"));
    }

}
