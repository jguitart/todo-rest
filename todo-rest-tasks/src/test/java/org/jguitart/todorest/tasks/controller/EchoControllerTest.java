package org.jguitart.todorest.tasks.controller;

import org.jguitart.todorest.tasks.IntegrationBaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class EchoControllerTest extends IntegrationBaseTest {


    @Test
    public void testEcho() {
        String text = String.format("Hi_%d", System.currentTimeMillis());
        String url = String.format("/api/echo/%s", text);
        given().port(port).when().get(url).then().body("message", is(text));
    }

}
