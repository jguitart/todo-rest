#!/bin/ash

if [ -z "$TASK_ENDPOINT" ]
then
      TASK_ENDPOINT_OPTION=""
else
      TASK_ENDPOINT_OPTION=" -Dtodo.api.task.endpoint=$TASK_ENDPOINT"
fi

if [ -z "$AUTH_ENDPOINT" ]
then
      AUTH_ENDPOINT_OPTION=""
else
      AUTH_ENDPOINT_OPTION=" -Dtodo.api.auth.endpoint=$AUTH_ENDPOINT"
fi



JAVA_OPTIONS = "$PORT_OPTION $MONGO_URI_OPTION"

java -jar $JAVA_OPTIONS todo-rest-auth.jar
