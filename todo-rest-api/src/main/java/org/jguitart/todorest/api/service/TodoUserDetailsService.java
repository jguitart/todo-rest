package org.jguitart.todorest.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TodoUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private TodoUserService todoUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return getTodoUserService().findByUsername(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TodoUserService getTodoUserService() {
        return todoUserService;
    }
}
