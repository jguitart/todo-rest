package org.jguitart.todorest.api.model;

public class TodoApiException extends Exception {

    public TodoApiException(String s) {
        super(s);
    }
}
