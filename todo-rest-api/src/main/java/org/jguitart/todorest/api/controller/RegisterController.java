package org.jguitart.todorest.api.controller;

import org.jguitart.todorest.api.model.TodoUser;
import org.jguitart.todorest.api.service.TodoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class RegisterController {

    @Autowired
    TodoUserService service;

    @RequestMapping(path ="/register", method = RequestMethod.POST)
    public TodoUser create(@RequestBody TodoUser user) throws IOException {
        TodoUser result = service.create(user);
        result.setPassword(null);
        return result;
    }

    public TodoUserService getService() {
        return service;
    }
}
