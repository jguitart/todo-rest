package org.jguitart.todorest.api.model;

import org.springframework.security.core.GrantedAuthority;

public class TodoGrantedAuthority implements GrantedAuthority {

    private String authority;

    public TodoGrantedAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
