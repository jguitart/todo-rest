package org.jguitart.todorest.api.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.jguitart.todorest.api.model.TodoUser;
import org.jguitart.todorest.api.service.HttpClient;
import org.jguitart.todorest.model.auth.User;
import org.jguitart.todorest.model.auth.UserImpl;
import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TodoUserRepository extends HttpClient {

    @Value("${todo.api.auth.endpoint}")
    private String restEndPoint;

    ObjectMapper mapper = new ObjectMapper();

    public TodoUser findByUsername(String username) throws IOException {
        final String url = String.format("%s/api/user/search", getRestEndPoint());

        SearchParams params = new SearchParams();
        params.setSize(1);
        List<SearchFilter> filters = new ArrayList<>();
        SearchFilter filter = new SearchFilter();
        filter.setName("withUsername");
        filter.setValue(username);
        filters.add(filter);
        params.setFilters(filters);

        SearchPage<TodoUser> data = this.search(params);
        TodoUser result = null;
        if(data!=null && data.getItems()!=null && !data.getItems().isEmpty()) {
            result = data.getItems().get(0);
        }

        return result;
    }


    public SearchPage<TodoUser> search(SearchParams params) throws IOException {

        final String url = String.format("%s/api/user/search", getRestEndPoint());
        String payload = mapper.writeValueAsString(params);
        Response response = doPost(url, payload);
        String json = response.body().string();
        SearchPage<User> data = mapper.readValue(json, new TypeReference<SearchPage<UserImpl>>() {});
        SearchPage<TodoUser> result = new SearchPage<>();
        result.setCount(data.getCount());
        result.setItems(data.getItems().stream().map(TodoUser::new).collect(Collectors.toList()));
        return result;
    }

    public TodoUser save(TodoUser user) throws IOException {
        final String url = String.format("%s/api/user", getRestEndPoint());
        String payload = mapper.writeValueAsString(user);
        Response response = doPost(url, payload);
        String json = response.body().string();
        User data = mapper.readValue(json, UserImpl.class);
        TodoUser result = new TodoUser(data);
        return result;
    }

    public TodoUser get(String id) throws IOException {
        final String url = String.format("%s/api/user/%s", getRestEndPoint(), id);
        Response response = doGet(url);
        String json = response.body().string();
        User data = mapper.readValue(json, UserImpl.class);
        TodoUser result = new TodoUser(data);
        return result;

    }

    public void delete(String id) throws IOException {
        final String url = String.format("%s/api/user/%s", getRestEndPoint(), id);
        doDelete(url);
    }

    private String getRestEndPoint() {
        return restEndPoint;
    }



}
