package org.jguitart.todorest.api.service;

import org.jguitart.todorest.api.model.TodoApiException;
import org.jguitart.todorest.api.model.TodoUser;
import org.jguitart.todorest.api.repository.TodoUserRepository;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class TodoUserService {

    @Autowired
    TodoUserRepository repository;

    public static final String DEFAULT_USER ="todoUser";
    public static final String DEFAULT_PASSWORD ="password";

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostConstruct
    public void initUser() throws IOException {

        TodoUser defaultUser = this.repository.findByUsername("todoUser");
        if(defaultUser ==null) {
            TodoUser user = new TodoUser();
            user.setUsername(DEFAULT_USER);
            user.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
            this.repository.save(user);
        }

    }

    public TodoUser create(TodoUser user) throws IOException {
        user.setId(null);
        String encodedPassword = this.getPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodedPassword);
        TodoUser result = this.repository.save(user);
        return result;
    }

    public TodoUser update(TodoUser user, String username) throws IOException, TodoApiException {
        TodoUser instance = this.get(user.getId());
        if(instance == null) {
            throw new TodoApiException("error.user.not_exists");
        }
        TodoUser principal = this.findByUsername(username);
        if(!instance.getId().equals(principal.getId())) {
            throw new TodoApiException("error.operation.not_allowed");
        }
        instance.setUsername(user.getUsername());
        TodoUser result = this.repository.save(instance);
        return result;
    }

    public TodoUser changeUserPassword(String username, String password) throws IOException, TodoApiException {
        TodoUser instance = this.findByUsername(username);
        return this.changePassword(instance, password);
    }

    public TodoUser changePassword(String id, String password) throws IOException, TodoApiException {
        TodoUser instance = this.get(id);
        return this.changePassword(instance, password);
    }

    public TodoUser changePassword(TodoUser user, String password) throws IOException, TodoApiException {
        if(user == null) {
            throw new TodoApiException("error.user.not_exists");
        }
        String encodedPassword = this.getPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodedPassword);
        TodoUser result = this.repository.save(user);
        return result;
    }

    public TodoUser get(String id) throws IOException {
        return this.repository.get(id);
    }

    public TodoUser findByUsername(String username) throws IOException {
        return this.repository.findByUsername(username);
    }

    public void delete(String id) throws IOException {
        this.repository.delete(id);
    }

    public SearchPage<TodoUser> search(SearchParams params) throws IOException {
        return this.repository.search(params);
    }

    public UserDetails getDetails(String username) throws IOException {
        return this.repository.findByUsername(username);
    }

    private TodoUserRepository getRepository() {
        return repository;
    }

    private PasswordEncoder getPasswordEncoder() {
        return this.passwordEncoder;
    }
}
