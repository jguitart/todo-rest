package org.jguitart.todorest.api.controller;

import org.jguitart.todorest.api.model.TodoApiException;
import org.jguitart.todorest.api.model.TodoUser;
import org.jguitart.todorest.api.repository.TodoUserRepository;
import org.jguitart.todorest.api.service.TodoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping("/api/user")
public class TodoUserController {

    @Autowired
    TodoUserService service;

    @RequestMapping(path ="/change-password", method = RequestMethod.PUT)
    public void changePassword(@RequestBody String password, Principal principal) throws IOException, TodoApiException {
        this.getService().changeUserPassword(principal.getName(), password);
    }

    @RequestMapping(path ="", method = RequestMethod.PUT)
    public TodoUser update(@RequestBody TodoUser user, Principal principal) throws IOException, TodoApiException {
        TodoUser result = this.getService().update(user, principal.getName());
        result.setPassword(null);
        return result;
    }

    @RequestMapping(path ="/me", method = RequestMethod.GET)
    public TodoUser getUserData(Principal principal) throws IOException, TodoApiException {
        TodoUser result = this.getService().findByUsername(principal.getName());
        if(result == null) {
            throw new TodoApiException("operation.not_allowed");
        }
        result.setPassword(null);
        return result;
    }


    public TodoUserService getService() {
        return service;
    }
}
