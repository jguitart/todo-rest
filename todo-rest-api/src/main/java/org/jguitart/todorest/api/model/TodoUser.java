package org.jguitart.todorest.api.model;

import org.jguitart.todorest.model.auth.User;
import org.jguitart.todorest.model.auth.UserImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TodoUser extends UserImpl implements UserDetails {

    public TodoUser() {

    }

    public TodoUser(User user) {
        this.apply(user);
    }

    public void apply(User user) {
        this.setUsername(user.getUsername());
        this.setPassword(user.getPassword());
        this.setLastUpdate(user.getLastUpdate());
        this.setId(user.getId());
        this.setCreatedDate(user.getCreatedDate());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> result = new ArrayList<>();
        result.add(new TodoGrantedAuthority("USER"));
        return result;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
