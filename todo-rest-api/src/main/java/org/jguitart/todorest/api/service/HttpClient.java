package org.jguitart.todorest.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.jguitart.todorest.api.model.HttpError;

import java.io.IOException;

public abstract class HttpClient {

    private ObjectMapper mapper = new ObjectMapper();

    public Response doPost(String url, String jsonPayload) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonPayload);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if(response.code()!=200) {
            String message = getErrorMessage(response);
            throw new IOException(message);
        }
        return response;
    }


    public Response doPut(String url, String jsonPayload) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonPayload);
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if(response.code()!=200) {
            String message = getErrorMessage(response);
            throw new IOException(message);
        }
        return response;
    }

    public Response doGet(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if(response.code()!=200) {
            String message = getErrorMessage(response);
            throw new IOException("error.connect");
        }
        return response;
    }

    public Response doDelete(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if(response.code()!=200) {
            String message = getErrorMessage(response);
            throw new IOException(message);
        }
        return response;
    }

    private String getErrorMessage(Response response) throws IOException {
        String error = "network.error";
        if(response.body()!= null) {
            String json = response.body().string();
            HttpError httpError = mapper.readValue(json, HttpError.class);
            error = httpError.getMessage();
        }
        return error;
    }

}
