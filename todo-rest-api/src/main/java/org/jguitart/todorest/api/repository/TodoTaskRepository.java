package org.jguitart.todorest.api.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.jguitart.todorest.api.service.HttpClient;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.model.task.TodoTask;
import org.jguitart.todorest.model.task.TodoTaskImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.stream.Collectors;

@Component
public class TodoTaskRepository extends HttpClient {

    @Value("${todo.api.task.endpoint}")
    private String restEndPoint;

    private ObjectMapper mapper = new ObjectMapper();

    public SearchPage<TodoTask> search(SearchParams params) throws IOException {
        final String url = String.format("%s/api/task/search", getRestEndPoint());
        String payload = mapper.writeValueAsString(params);
        Response response = doPost(url, payload);
        String json = response.body().string();
        SearchPage<TodoTask> result = mapper.readValue(json, new TypeReference<SearchPage<TodoTaskImpl>>() {});
        return result;
    }

    public TodoTask save(TodoTask task) throws IOException {
        TodoTask result = null;
        if(task.getId()==null) {
            result = this.create(task);
        } else {
            result = this.update(task);
        }
        return result;
    }

    public TodoTask create(TodoTask task) throws IOException {
        final String url = String.format("%s/api/task", getRestEndPoint());
        String payload = mapper.writeValueAsString(task);
        Response response = doPost(url, payload);
        String json = response.body().string();
        TodoTask result = mapper.readValue(json, TodoTaskImpl.class);
        return result;
    }

    public TodoTask update(TodoTask task) throws IOException {
        final String url = String.format("%s/api/task", getRestEndPoint());
        String payload = mapper.writeValueAsString(task);
        Response response = doPut(url, payload);
        String json = response.body().string();
        TodoTask result = mapper.readValue(json, TodoTaskImpl.class);
        return result;
    }

    public void delete(String id) throws IOException {
        final String url = String.format("%s/api/task/%s", getRestEndPoint(), id);
        doDelete(url);
    }

    public TodoTask get(String id) throws IOException {
        final String url = String.format("%s/api/task/%s", getRestEndPoint(), id);
        Response response = doGet(url);
        TodoTask result = null;
        if(response.body()!=null) {
            String json = response.body().string();
            result = mapper.readValue(json, TodoTaskImpl.class);
        }
        return result;
    }

    public String getRestEndPoint() {
        return restEndPoint;
    }
}
