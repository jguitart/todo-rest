package org.jguitart.todorest.api.controller;

import org.jguitart.todorest.api.model.TodoApiException;
import org.jguitart.todorest.api.service.TodoTaskService;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.model.task.TodoTask;
import org.jguitart.todorest.model.task.TodoTaskImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping("/api/task")
public class TodoTaskController {

    @Autowired
    TodoTaskService service;

    @RequestMapping(path ="/search", method = RequestMethod.POST)
    public SearchPage<TodoTask> search(@RequestBody SearchParams params, Principal principal) throws IOException {
         return getService().searchOwn(params, principal.getName());
    }

    @RequestMapping(path ="", method = RequestMethod.POST)
    public TodoTask create(@RequestBody TodoTaskImpl task, Principal principal) throws IOException {
        return getService().create(task, principal.getName());
    }

    @RequestMapping(path ="", method = RequestMethod.PUT)
    public TodoTask update(@RequestBody TodoTaskImpl task, Principal principal) throws IOException, TodoApiException {
        return getService().update(task, principal.getName());
    }

    @RequestMapping(path ="/{id}/{completed}", method = RequestMethod.GET)
    public TodoTask completed(@PathVariable String id, @PathVariable boolean completed, Principal principal) throws IOException, TodoApiException {
        return getService().setCompleted(id, completed, principal.getName());
    }

    @RequestMapping(path ="/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id, Principal principal) throws IOException, TodoApiException {
        getService().delete(id, principal.getName());
    }

    public TodoTaskService getService() {
        return service;
    }
}
