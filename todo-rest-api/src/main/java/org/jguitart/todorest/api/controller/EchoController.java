package org.jguitart.todorest.api.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

@RestController
@RequestMapping("/public/echo")
public class EchoController {

    @RequestMapping(path = "/{message}", method = RequestMethod.GET)
    public LinkedHashMap<String, String> echo(@PathVariable String message) throws Exception {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        result.put("message", message);
        result.put("timestamp", String.valueOf(System.currentTimeMillis()));
        return result;
    }

}
