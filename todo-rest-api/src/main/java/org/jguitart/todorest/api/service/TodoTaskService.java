package org.jguitart.todorest.api.service;

import org.jguitart.todorest.api.model.TodoApiException;
import org.jguitart.todorest.api.model.TodoUser;
import org.jguitart.todorest.api.repository.TodoTaskRepository;
import org.jguitart.todorest.model.search.SearchFilter;
import org.jguitart.todorest.model.search.SearchPage;
import org.jguitart.todorest.model.search.SearchParams;
import org.jguitart.todorest.model.task.TodoTask;
import org.jguitart.todorest.model.task.TodoTaskImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TodoTaskService {

    @Autowired
    private TodoUserService userService;

    @Autowired
    private TodoTaskRepository repository;

    public SearchPage<TodoTask> searchOwn(SearchParams params, String username) throws IOException {
        TodoUser user = getUserService().findByUsername(username);

        SearchFilter userfilter = new SearchFilter();
        userfilter.setName("byUserId");
        userfilter.setValue(user.getId());
        SearchPage<TodoTask> result = this.getRepository().search(params);
        return result;
    }

    public TodoTask create(TodoTask task, String username) throws IOException {
        TodoUser user = getUserService().findByUsername(username);
        TodoTaskImpl instance = new TodoTaskImpl();
        instance.setCompleted(task.isCompleted());
        instance.setText(task.getText());
        instance.setUserId(user.getId());

        TodoTask result = this.getRepository().create(instance);
        return result;
    }

    public TodoTask update(TodoTask task, String username) throws IOException, TodoApiException {
        TodoUser user = getUserService().findByUsername(username);
        if(!user.getId().equals(task.getUserId())) {
            throw new TodoApiException("operation.not_allowed");
        }
        TodoTask result = this.getRepository().create(task);
        return result;
    }

    public TodoUserService getUserService() {
        return userService;
    }

    public TodoTaskRepository getRepository() {
        return repository;
    }

    public TodoTask setCompleted(String id, boolean completed, String username) throws IOException, TodoApiException {
        TodoUser user = getUserService().findByUsername(username);
        TodoTask instance = this.getRepository().get(id);
        this.isOwner(instance, user);

        TodoTaskImpl task = new TodoTaskImpl();

        task.setId(instance.getId());
        task.setText(instance.getText());
        task.setCompleted(completed);
        task.setUserId(instance.getUserId());

        TodoTask result = this.getRepository().save(task);
        return result;
    }

    public void delete(String id, String username) throws IOException, TodoApiException {
        TodoUser user = getUserService().findByUsername(username);
        TodoTask instance = this.getRepository().get(id);
        this.isOwner(instance, user);

        this.getRepository().delete(instance.getId());
    }

    private void isOwner(TodoTask task, TodoUser user) throws TodoApiException {
        if(!user.getId().equals(task.getUserId())) {
            throw new TodoApiException("operation.not_allowed");
        }
    }
}
