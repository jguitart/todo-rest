# Todo Rest API

Introduction
------------

This is the API gateway of the todo-rest application. 
This service is the responsible of authentication and authorization of users, and the entrypoint of the system.

Authorization user with jwt (using oauth2 direct authentication) wich let all requests be stateless for future
multi-instance/multi-container architecure of the service.

Dockerfile
----------

 