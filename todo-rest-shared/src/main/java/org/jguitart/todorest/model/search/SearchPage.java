package org.jguitart.todorest.model.search;

import java.util.List;

public class SearchPage<T> {

    private long count;
    private List<T> items;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
