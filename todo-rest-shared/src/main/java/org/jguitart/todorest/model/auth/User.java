package org.jguitart.todorest.model.auth;

import java.util.Date;

public interface User {

    String getId();
    String getUsername();
    String getPassword();

    Date getCreatedDate();
    Date getLastUpdate();


}
