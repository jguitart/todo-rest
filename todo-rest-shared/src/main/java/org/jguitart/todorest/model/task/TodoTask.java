package org.jguitart.todorest.model.task;

import java.util.Date;

public interface TodoTask {

    String getId();

    String getText();

    boolean isCompleted();

    String getUserId();

    Date getCreatedDate();

    Date getLastUpdate();
}
